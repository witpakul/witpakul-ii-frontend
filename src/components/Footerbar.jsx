import React, { Component } from 'react';
import { MDBContainer, MDBFooter } from "mdbreact";

class Footerbar extends Component {
  render() {
    return (
      <MDBFooter color="default-color" className="font-small mt-4">
        <div className="footer-copyright text-center py-3">
          <MDBContainer fluid>
            &copy; {new Date().getFullYear()} Copyright: 
            <a className="text-white font-weight-light" href="https://www.youtube.com/watch?v=QhBnZ6NPOY0"> WitPaKul Shop II</a>
          </MDBContainer>
        </div>
      </MDBFooter>
    );
  }
}

export default Footerbar;
